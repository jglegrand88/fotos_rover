import requests
import webbrowser


def request_photos(url, api_key, page='1'):
    # funcion que obtiene json de respuesta desde la api de la nasa con información del rover,
    # camaras y fotos para luego crear
    # un diccionario con los datos del json y retornarlo como respuesta.
    var_url = url+'&page='+page+'&api_key='+api_key
    print(var_url)
    api_response = requests.get(var_url)
    if not api_response.ok:
        print("Error en la petición a API de NASA:".format(api_response.reason))
        return None
    photos = api_response.json()["photos"]
    diccionario = {}
    indice = 0
    for photo in photos:
        id = photo["id"]
        sol = photo["sol"]
        camera = {'id': photo['camera']['id'], 'name': photo['camera']['name'], 'rover_id': photo['camera']['rover_id']}
        img_src = photo["img_src"]
        earth_date = photo["earth_date"]
        rover = {'id': photo['rover']['id'], 'name': photo['rover']['name'], 'landing_date': photo['rover']['landing_date'], 'launch_date': photo['rover']['launch_date'], 'status': photo['rover']['status'], 'max_sol': photo['rover']['max_sol'], 'max_date': photo['rover']['max_date'], 'total_photos': photo['rover']['total_photos']}
        photos_dict = {}
        photos_dict['id'] = id
        photos_dict['sol'] = sol
        photos_dict['camera'] = camera
        photos_dict['img_src'] = img_src
        photos_dict['earth_date'] = earth_date
        photos_dict['rover'] = rover
        diccionario[str(indice)] = photos_dict
        indice += 1
    return diccionario


def build_web_page():
    response = request_photos('https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000','DEMO_KEY','26')
    print (response)
    block1 = """<html>
                 <head>
                 </head>
                 <body>
                     <ul>"""
    block2 = ""
    for photo in response.values():
        block2 += "<li><img src='" + photo['img_src'] + "'></li>"
    block3 = """</ul>
                </body>
            </html>"""
    html = block1+block2+block3
    f = open('fotosRover.html', 'w')
    f.write(html)
    f.close()
    webbrowser.open_new_tab('fotosRover.html')


build_web_page()